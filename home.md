# Home

## Subtitle

In the words of Abraham Lincoln:

> Pardon my French

Use `git status` to list all new or modified files that haven't yet been committed.

Some basic Git commands are:

```
git status
git add
git commit
```

Some javascript:

```javascript
function fancyAlert(arg) {
  if(arg) {
    $.facebox({div:'#foo'})
  }
}
```

This is a task list:

- [x] @mentions, #refs, [links](https://google.com), *formatting*, and <del>tags</del> supported
- [x] list syntax required (any unordered or ordered list supported)
- [x] this is a complete item
- [ ] this is an incomplete item

## Table

This is a section with a table

First Header | Second Header
------------ | -------------
Content from cell 1 | Content from cell 2
Content in the first column | Content in the second column

## Image

Please look at the puppy below.

![dog not found](assets/dog.jpg "The Dog")